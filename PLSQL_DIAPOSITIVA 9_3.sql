/* */
SELECT
    object_type,
    object_name
FROM
    user_objects;
/

/* VER LOS OBJETOS Y ORDENADOS POR TIPO*/
SELECT
    object_type,
    COUNT(*)
FROM
    user_objects
GROUP BY
    object_type;
/

/**/
SELECT
    COUNT(*)
FROM
    dict/* WHERE table_name LIKE 'USER%'*/;
/

CREATE OR REPLACE PROCEDURE add_department_noex (
    p_name  VARCHAR2,
    p_mgr   NUMBER,
    p_loc   NUMBER
) IS
BEGIN
    INSERT INTO departments (
        department_id,
        department_name,
        manager_id,
        location_id
    ) VALUES (
        DEPARTMENTS_SEQ.NEXTVAL,
        p_name,
        p_mgr,
        p_loc
    );

    dbms_output.put_line('Added Dept: ' || p_name);
END;
/

BEGIN
    add_department_noex('Media', 100, 1800);
    add_department_noex('Editing', 99, 1800);
    add_department_noex('Advertising', 101, 1800);
END;