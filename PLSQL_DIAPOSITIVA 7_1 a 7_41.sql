DECLARE
    CURSOR cur_emps IS
    SELECT
        employee_id,
        last_name
    FROM
        all_50s
    WHERE
        department_id = 50;

    v_last_name_old last_name%rowtype;
BEGIN FOR v_emp_record IN cur_emps LOOP UPDATE all_50s set last-name
    
        dbms_output.put_line(v_emp_record.employee_id
                             || ' '
                             || v_emp_record.last_name);
    END LOOP;
END;
/ DECLARE
    v_country_name  countries.country_name%TYPE := 'Korea, South';
    v_reg_id
    countries.region_id%TYPE;

BEGIN
    SELECT
        region_id
    INTO v_reg_id
    FROM
        countries
    WHERE
        country_name = v_country_name;

EXCEPTION
    WHEN no_data_found THEN --Si el registro no tiene datos
                dbms_output.put_line('Country name, '
                             || v_country_name
                             || ',
        cannot be found. Re-enter the country name using the correct
        spelling.');
END;
/

/* MANEJO DE EXCEPCIONES */ DECLARE
    v_lname
    employees.last_name%TYPE;

BEGIN
    SELECT
        last_name
    INTO v_lname
    FROM
        employees
    WHERE
        job_id = 'ST_CLERK';

    dbms_output.put_line('The last name of the ST_CLERK is: ' || v_lname);
EXCEPTION
    WHEN too_many_rows THEN
        dbms_output.put_line('Your select statement retrieved multiple rows.
Consider using a cursor.');
END;
/

/* CREAR NUESTRAS PROPIAS EXCEPCIONES */
DECLARE
    e_insert_excep EXCEPTION;
    PRAGMA exception_init ( e_insert_excep, -01400 );
BEGIN
    INSERT INTO departments (
        department_id,
        department_name
    ) VALUES (
        280,
        NULL
    );

EXCEPTION
    WHEN e_insert_excep THEN
        dbms_output.put_line('INSERT FAILED');
END;

/* TRAPPING User-Defined Exceptions */
DECLARE
    e_invalid_department EXCEPTION;
    v_name    VARCHAR2(20) := 'Accounting';
    v_deptno  NUMBER := 27;
BEGIN
    UPDATE departments
    SET
        department_name = v_name
    WHERE
        department_id = v_deptno;

    IF SQL%notfound THEN
        RAISE e_invalid_department;
    END IF;
EXCEPTION
    WHEN e_invalid_department THEN
        dbms_output.put_line('No such department id.');
END;
/

/*  */
DECLARE
    v_mgr          PLS_INTEGER := 27;
    v_employee_id  employees.employee_id%TYPE;
BEGIN
    SELECT
        employee_id
    INTO v_employee_id
    FROM
        employees
    WHERE
        manager_id = v_mgr;

    dbms_output.put_line('Employee #'
                         || v_employee_id
                         || ' works for manager #'
                         || v_mgr
                         || '.');

EXCEPTION
    WHEN no_data_found THEN
        raise_application_error(-20201, 'This manager has no employees');
    WHEN too_many_rows THEN
        raise_application_error(-20202, 'Too many employees were found.');
END;
/
/**/ DECLARE
    v_last_name
    employees.last_name%TYPE;

BEGIN
    BEGIN
        SELECT
            last_name
        INTO v_last_name
        FROM
            employees
        WHERE
            employee_id = 999;

        dbms_output.put_line('Message 1');
    EXCEPTION
        WHEN too_many_rows THEN
            dbms_output.put_line('Message 2');
    END;

    dbms_output.put_line('Message 3');
EXCEPTION
    WHEN OTHERS THEN
        dbms_output.put_line('Message 4');
END;
/

/*  */
DECLARE
    e_myexcep EXCEPTION;
BEGIN
    BEGIN
        RAISE e_myexcep;
        dbms_output.put_line('Message 1');
    EXCEPTION
        WHEN too_many_rows THEN
            dbms_output.put_line('Message 2');
    END;

    dbms_output.put_line('Message 3');
EXCEPTION
    WHEN e_myexcep THEN
        dbms_output.put_line('Message 4');
END;
/

/**/
DECLARE
    e_myexcep EXCEPTION;
BEGIN
    BEGIN
        RAISE e_myexcep;
        dbms_output.put_line('Message 1');
    EXCEPTION
        WHEN too_many_rows THEN
            dbms_output.put_line('Message 2');
    END;

    dbms_output.put_line('Message 3');
EXCEPTION
    WHEN no_data_found THEN
        dbms_output.put_line('Message 4');
END;