CREATE OR REPLACE PACKAGE our_exceptions IS
e_cons_violation
EXCEPTION;
PRAGMA EXCEPTION_INIT (e_cons_violation, -2292);
e_value_too_large
EXCEPTION;
PRAGMA EXCEPTION_INIT (e_value_too_large, -1438);
END our_exceptions;
/

CREATE OR REPLACE PACKAGE global_consts IS
mile_to_kilo
CONSTANT NUMBER := 1.6093;
kilo_to_mile
CONSTANT NUMBER := 0.6214;
yard_to_meter
CONSTANT NUMBER := 0.9144;
meter_to_yard
CONSTANT NUMBER := 1.0936;
END global_consts;
/

DECLARE
distance_in_miles NUMBER(5) := 5000;
distance_in_kilo
NUMBER(6,2);
BEGIN
distance_in_kilo :=
distance_in_miles * global_consts.mile_to_kilo;
DBMS_OUTPUT.PUT_LINE(distance_in_kilo);
END;
/

BEGIN
INSERT INTO excep_test (number_col) VALUES (999);
EXCEPTION
WHEN our_exceptions.e_value_too_large THEN
DBMS_OUTPUT.PUT_LINE('Value too big for column data
type');
END;
/

CREATE TABLE excep_test (number_col NUMBER(3));
/
SELECT * FROM excep_test;
/

CREATE OR REPLACE PACKAGE taxes_pkg IS
FUNCTION tax (p_value IN NUMBER) RETURN NUMBER;
END taxes_pkg;
/

CREATE OR REPLACE PACKAGE BODY taxes_pkg IS
FUNCTION tax (p_value IN NUMBER) RETURN NUMBER IS
v_rate NUMBER := 0.08;
BEGIN
RETURN (p_value * v_rate);
END tax;
END taxes_pkg;
/

SELECT taxes_pkg.tax(salary), salary, last_name
FROM employees;
/

CREATE OR REPLACE PROCEDURE sel_one_emp
(p_emp_id
IN employees.employee_id%TYPE,
p_emprec
OUT employees%ROWTYPE)
IS BEGIN
SELECT * INTO p_emprec FROM employees
WHERE employee_id = p_emp_id;
END sel_one_emp;
/

DECLARE
v_emprec
employees%ROWTYPE;
BEGIN
sel_one_emp(120, v_emprec);
dbms_output.put_line(v_emprec.last_name);
END;


