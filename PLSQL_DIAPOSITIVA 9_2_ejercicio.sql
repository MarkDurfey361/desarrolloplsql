/* reparar y evitar el error de mutating table */
CREATE OR REPLACE FUNCTION query_max_sal (
    p_dept_id NUMBER
) RETURN NUMBER IS
    v_num NUMBER;
BEGIN
    SELECT
        MAX(salary)
    INTO v_num
    FROM
        employees
    WHERE
        department_id = p_dept_id;

    return(v_num);
END;
/

/* */
UPDATE employees
SET salary = query_max_sal(department_id)
WHERE employee_id = 174;