/* VARIABLES EN CURSORES DE TIPO ESTRUCTURA DE UNA TABLA */
/*  */
DECLARE
    CURSOR cur_emps IS
    SELECT
        *
    FROM
        employees
    WHERE
        department_id = 30;
/*SE INDICA EL TIPO DE LA VARIABLE QUE ES IGUAL
A EMPLOYEES*/
                    v_emp_record cur_emps%rowtype;
BEGIN
    OPEN cur_emps;
    LOOP
        FETCH cur_emps INTO v_emp_record;-- CADA REGISTRO ITERADO SE GUARDA EN v_emp_record
                EXIT WHEN cur_emps%notfound;-- EXIT LOOP WHEN YA NO HAYA REGISTROS QUE ITERAR
                dbms_output.put_line(v_emp_record.employee_id
                             || ' - '
                             || v_emp_record.last_name
                             || ' - '
                             || v_emp_record.salary);

    END LOOP;

    CLOSE cur_emps;
END;
/

/* CURSOR USANDO DOS TABLAS */
DECLARE
    CURSOR cur_emps_dept IS -- CURSOR A BASE DE DOS TABLAS
            SELECT
        first_name,
        last_name,
        department_name
    FROM
        employees    e,
        departments  d
    WHERE
        e.department_id = d.department_id;

    v_emp_dept_record cur_emps_dept%rowtype; -- ESTA VARIABLE TIENE LA ESTRUCTURA DE CURSOR
BEGIN
    OPEN cur_emps_dept;
    LOOP
        FETCH cur_emps_dept INTO v_emp_dept_record;
        EXIT WHEN cur_emps_dept%notfound;
        dbms_output.put_line(v_emp_dept_record.first_name
                             || ' � '
                             || v_emp_dept_record.last_name
                             || ' � '
                             || v_emp_dept_record.department_name);

    END LOOP;

    CLOSE cur_emps_dept;
END;
/

/*
NOTA: PARA USAR CUR_EMPS%ROWCOUNT EN UN INSERT NO SE PUEDE
DEBE GUARDARSE PRIMERO EN UNA VARIABLE Y LUEGO SE USA EN EL INSERT
*/

/* LO QUE YA VIMOS EN LOS OTROS EJERCICIOS
PERO ESTA VEZ LA CONDICI�N DE SALIDA DEL LOOP ES
QUE SI EL N�MERO DE REGISTROS ITERADOS ES MAYOR
A 10, O SI YA NO HAY REGISTROS QU� ITERAR.*/
DECLARE
    CURSOR cur_emps IS
    SELECT
        employee_id,
        last_name
    FROM
        employees;

    v_emp_record  cur_emps%rowtype;-- LA VARIABLE ADOOPTA EL TIPO DEL REGISTRO OBTENIDO (EMPLOYEE)
        v_registros   NUMBER;
BEGIN
    OPEN cur_emps;
    LOOP
        FETCH cur_emps INTO v_emp_record;
        EXIT WHEN /*cur_emps%rowcount > 10 OR*/ cur_emps%notfound;
        v_registros := cur_emps%rowcount;
        dbms_output.put_line(v_emp_record.employee_id
                             || ' '
                             || v_emp_record.last_name
                             || ' '
                             || 'Vamos en el '
                             || v_registros);

    END LOOP;

    CLOSE cur_emps;
END;