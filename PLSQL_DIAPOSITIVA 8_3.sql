/*  */
CREATE OR REPLACE PROCEDURE query_emp (
    p_id      IN   employees.employee_id%TYPE,
    p_name    OUT  employees.last_name%TYPE,
    p_salary  OUT  employees.salary%TYPE
) IS
BEGIN
    SELECT
        last_name,
        salary
    INTO
        p_name,
        p_salary
    FROM
        employees
    WHERE
        employee_id = p_id;

END query_emp;
/

/**/
DECLARE
    a_emp_name  employees.last_name%TYPE;
    a_emp_sal   employees.salary%TYPE;
BEGIN
    query_emp(178, a_emp_name, a_emp_sal);
    dbms_output.put_line('Name: ' || a_emp_name);
    dbms_output.put_line('Salary: ' || a_emp_sal);
END;
/

/**/
CREATE OR REPLACE PROCEDURE format_phone (
    p_phone_no IN OUT VARCHAR2
) IS
BEGIN
    p_phone_no := '('
                  || substr(p_phone_no, 1, 3)
                  || ')'
                  || substr(p_phone_no, 4, 3)
                  || '-'
                  || substr(p_phone_no, 7);
END format_phone;
/

/**/
DECLARE
    a_phone_no VARCHAR2(13);
BEGIN
    a_phone_no := '3328075254';
    format_phone(a_phone_no);
    dbms_output.put_line('The formatted number is: ' || a_phone_no);
END;