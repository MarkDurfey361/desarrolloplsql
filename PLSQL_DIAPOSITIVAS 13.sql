/* DISPARADORES */ --Ejemplo Update
CREATE OR REPLACE TRIGGER log_sal_change_trigg AFTER
    UPDATE OF salary ON employees
BEGIN
    INSERT INTO log_table (
        user_id,
        logon_date
    ) VALUES (
        user,
        sysdate
    );

END;
/

CREATE TABLE log_table (
    user_id     NUMBER NOT NULL,
    logon_date  DATE,
    CONSTRAINT log_table_pk PRIMARY KEY ( user_id ) ENABLE
);
/

DROP TABLE log_table;
/

CREATE TABLE log_table (
    user_id     VARCHAR2(100)
        NOT NULL ENABLE,
    logon_date  DATE
);
/

UPDATE employees
SET
    salary = salary * 1.1
WHERE
    employee_id = 120;
/

SELECT
    *
FROM
    log_table;

--FIN EJEMPLO UPDATE