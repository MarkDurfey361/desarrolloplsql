/*Bloque an�nimo*/
/* Esto es equivalente a un SOUTln */
BEGIN
    dbms_output.put_line('Hola Mundo!');
END;
/

/*Bloque an�nimo*/
/* Impresi�n de una variable tipo DATE que contiene la fecha */
DECLARE
    v_date DATE := sysdate + 1;
BEGIN
    dbms_output.put_line(v_date);
END;
/

/* Se declaran dos variable Y se les asignan valores 
import�ndolos de una tabla */
DECLARE
    v_first_name  VARCHAR2(25);
    v_last_name   VARCHAR2(25);
BEGIN
    SELECT
        first_name,
        last_name
    INTO
        v_first_name,
        v_last_name
    FROM
        employees
    WHERE
        last_name = 'Oswald';

    dbms_output.put_line('The employee of the month is: '
                         || v_first_name
                         || ' '
                         || v_last_name
                         || '.');

EXCEPTION
    WHEN too_many_rows THEN
        dbms_output.put_line('Your select statement retrieved
multiple rows. Consider using a cursor or changing
the search criteria.');
END;
/

/* 
Creaci�n de un procedimiento
Se defini� y se almacen� en la BD
Este bloque ya no es an�nimo, ya tiene nombre; "print_date"
*/
CREATE OR REPLACE PROCEDURE print_date IS
    v_date VARCHAR2(30); --Crea una variable
BEGIN
    SELECT
        to_char(sysdate + 1, 'Mon DD, YYYY') --Fecha del sistema mas 1 en el d�a
        INTO v_date
    FROM
        dual;

    dbms_output.put_line(v_date);
END;
/

/* EJECUTAR EL PROCEDIMIENTO ANTERIOR */
BEGIN
    print_date; --Se manda a llamar el procedimiento
END;
/

/* 
CREANDO UNA FUNCI�N QUE DEVUELVA QU� D�A ES MA�ANA
IN - DATOS QUE ENTRAN
OUT - DATOS QUE SALEN
*/
CREATE OR REPLACE FUNCTION tomorrow (
    p_today IN DATE -- Define que va a recibir un valor date y lo guardas en (IN) p_today
) RETURN DATE IS -- Se especifica qu� tipo de dato se va a devolver
    v_tomorrow DATE; -- Lo que contenga v_tomorrow que es date
BEGIN
    SELECT
        p_today + 1-- Haz select en p_todaY (LA FECHA DE MA�ANA)
    INTO v_tomorrow -- Donde se va a guardar
    FROM -- Desde donde va a hacer el select
        dual;

    RETURN v_tomorrow; -- Lo que va a retornar
END;
/

/* 
EJECUTAR LA FUNCI�N ANTERIOR FORMA 1
ESTE RESULTADO SALE EN CONSOLA
*/
SELECT TOMORROW(SYSDATE) AS "Tomorrow's Date"
FROM DUAL;
/

/* 
EJECUTAR LA FUNCI�N ANTERIOR FORMA 2
ESTE RESULTADO SALE EN DBMS
*/
BEGIN
DBMS_OUTPUT.PUT_LINE(TOMORROW(SYSDATE));
END;