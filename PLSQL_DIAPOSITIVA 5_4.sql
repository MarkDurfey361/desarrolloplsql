/* 
APLICAR CURSORES CON PAR�METROS
WEB SERVICE O APP MOVIL, EL USUARIO DECIDA QU� PA�SES DE QU� REGI�N
*/
DECLARE
    CURSOR cur_country (
        p_region_id NUMBER
    ) IS -- SE INDICA QUE SE RECIBE UN PAR�METRO
        SELECT
        country_id,
        country_name
    FROM
        countries
    WHERE
        region_id = p_region_id;-- SE PASA EL PAR�METRO

        v_country_record cur_country%rowtype;-- VARIABLE DEL TIPO DEL CURSOR
BEGIN
    OPEN cur_country(1);--ABRE EL CURSOR Y SE ASIGNA PAR�METRO
        LOOP
        FETCH cur_country INTO v_country_record;
        EXIT WHEN cur_country%notfound;
        dbms_output.put_line(v_country_record.country_id
                             || ' '
                             || v_country_record.country_name);
    END LOOP;

    CLOSE cur_country;
END;
/

/*  */
DECLARE
    CURSOR cur_countries (
        p_region_id   NUMBER,
        p_country_id  CHAR
    ) IS
    SELECT
        country_id,
        country_name
    FROM
        countries
    WHERE
        region_id = p_region_id
        OR country_id = 'BR';

BEGIN
    FOR v_country_record IN cur_countries(145, 'BR') LOOP
        dbms_output.put_line(v_country_record.country_id
                             || '----'
                             || v_country_record.country_name);
    END LOOP;
END;