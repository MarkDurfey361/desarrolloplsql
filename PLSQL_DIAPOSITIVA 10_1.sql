/* PAQUETES */

CREATE OR REPLACE PACKAGE check_emp_pkg IS
    g_max_length_of_service CONSTANT NUMBER := 100;
    PROCEDURE chk_hiredate (
        p_date IN employees.hire_date%TYPE
    );

    PROCEDURE chk_dept_mgr (
        p_empid  IN  employees.employee_id%TYPE,
        p_mgr    IN  employees.manager_id%TYPE
    );

END check_emp_pkg;
/

CREATE OR REPLACE PACKAGE manage_jobs_pkg IS
    g_todays_date DATE := sysdate;
    CURSOR jobs_curs IS
    SELECT
        employee_id,
        job_id
    FROM
        employees
    ORDER BY
        employee_id;

    PROCEDURE update_job (
        p_emp_id IN employees.employee_id%TYPE
    );

    PROCEDURE fetch_emps (
        p_job_id  IN   employees.job_id%TYPE,
        p_emp_id  OUT  employees.employee_id%TYPE
    );

END manage_jobs_pkg;
/

/* CUERPO DE PAQUETES */
CREATE OR REPLACE PACKAGE BODY check_emp_pkg IS

    PROCEDURE chk_hiredate (
        p_date IN employees.hire_date%TYPE
    ) IS
    BEGIN
        IF months_between(sysdate, p_date) > g_max_length_of_service * 12 THEN
            raise_application_error(-20200, 'Invalid Hiredate');
        END IF;
    END chk_hiredate;

    PROCEDURE chk_dept_mgr (
        p_empid  IN  employees.employee_id%TYPE,
        p_mgr    IN  employees.manager_id%TYPE
    ) IS
    BEGIN
        dbms_output.put_line(' ');
    END chk_dept_mgr;

END check_emp_pkg;
/

DESC check_emp_pkg;

CREATE OR REPLACE PACKAGE salary_pkg IS
    g_max_sal_raise CONSTANT NUMBER := 0.20;
    PROCEDURE update_sal (
        p_employee_id  employees.employee_id%TYPE,
        p_new_salary   employees.salary%TYPE
    );

END salary_pkg;
/

/*CUERPO DE PAQUETE */
CREATE OR REPLACE PACKAGE BODY salary_pkg IS

    FUNCTION validate_raise -- private function

     (
        p_old_salary  employees.salary%TYPE,
        p_new_salary  employees.salary%TYPE
    ) RETURN BOOLEAN IS
    BEGIN
        IF p_new_salary > ( p_old_salary * ( 1 + g_max_sal_raise ) ) THEN
            RETURN false;
        ELSE
            RETURN true;
        END IF;
    END validate_raise;

    PROCEDURE update_sal -- public procedure

     (
        p_employee_id  employees.employee_id%TYPE,
        p_new_salary   employees.salary%TYPE
    ) IS
        v_old_salary employees.salary%TYPE; -- local variable
        BEGIN
        SELECT
            salary
        INTO v_old_salary
        FROM
            employees
        WHERE
            employee_id = p_employee_id;

        IF validate_raise(v_old_salary, p_new_salary) THEN
            UPDATE employees
            SET
                salary = p_new_salary
            WHERE
                employee_id = p_employee_id;

        ELSE
            raise_application_error(-20210, 'Raise too high');
        END IF;

    END update_sal;

END salary_pkg;