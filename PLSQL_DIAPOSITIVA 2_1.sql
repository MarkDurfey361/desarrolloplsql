/* ESTO S�LO VA A FUNCIONAR SI DEVUELVE UN S�LO REGISTRO */
SELECT
    first_name,
    department_id
INTO
    v_emp_fname,
    v_emp_deptno
FROM
    �;
/

/* BLOQUE AN�NIMO QUE HAR� UN CONTADOR */
DECLARE
    v_counter INTEGER := 0;
BEGIN
    v_counter := v_counter + 1;
    dbms_output.put_line(v_counter);
END;
/

/*
NOTA: INVESTIGAR M�S SOBRE PLS_INTEGER DATA TYPE
*/

/* PROCEDIMIENTO QUE MUESTRA UN NOBRE (DBMS) DADO */
DECLARE
    v_myname VARCHAR2(20);
BEGIN
    dbms_output.put_line('My name is: ' || v_myname);
    v_myname := 'John';
    dbms_output.put_line('My name is: ' || v_myname);
END;
/

/**/
DECLARE
    v_date VARCHAR2(30);
BEGIN
    SELECT
        to_char(sysdate)
    INTO v_date
    FROM
        dual;

    dbms_output.put_line(v_date);
END;
/

/* N�MERO DE CARACTERES DE UN STRING */
CREATE FUNCTION num_characters (
    p_string IN VARCHAR2
) RETURN INTEGER IS
    v_num_characters INTEGER;
BEGIN
    SELECT
        length(p_string)
    INTO v_num_characters
    FROM
        dual;

    RETURN v_num_characters;
END;
/

/* PROBAR LA FUNCI�N ANTERIOR */
DECLARE
    v_length_of_string INTEGER;
BEGIN
    v_length_of_string := num_characters('Oracle
Corporation');
    dbms_output.put_line(v_length_of_string);
END;