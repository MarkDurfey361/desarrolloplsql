/* CURSORES ANHIDADOS */
DECLARE
    CURSOR cur_loc IS
    SELECT
        *
    FROM
        locations;

    CURSOR cur_dept (
        p_locid NUMBER
    ) IS
    SELECT
        *
    FROM
        departments
    WHERE
        location_id = p_locid;

BEGIN
    FOR v_locrec IN cur_loc LOOP
        dbms_output.put_line(v_locrec.city);
        FOR v_deptrec IN cur_dept(v_locrec.location_id) LOOP
            dbms_output.put_line(v_deptrec.department_name);
        END LOOP;

    END LOOP;
END;