/* BLOQUES INTERNOS O ANHIDADOS */
DECLARE
    v_father_name    VARCHAR2(20) := 'Patrick';
    v_date_of_birth  DATE := '20-Abril-1972';
BEGIN
    DECLARE
        v_child_name VARCHAR2(20) := 'Mike';
    BEGIN
        dbms_output.put_line('Father''s Name: ' || v_father_name);
        dbms_output.put_line('Date of Birth: ' || v_date_of_birth);
        dbms_output.put_line('Child''s Name: ' || v_child_name);
    END;
    --dbms_output.put_line('Child''s Name: ' || v_child_name); Esto no puede ser, porque el bloque externo no puede acceder al interno
                dbms_output.put_line('Date of Birth: ' || v_date_of_birth);
END;

/*
Nota: Los bloques externos no pueden acceder a la informaci�n de los internos,
pero los internos s� pueden acceder a los externos.
*/
/

/* ESTO FUNCIONAR�A? R = S� LO HAR�A */
DECLARE
    v_first_name  VARCHAR2(20);
    v_last_name   VARCHAR2(20);
BEGIN
    BEGIN
        v_first_name := 'Carmen';
        v_last_name := 'Miranda';
        dbms_output.put_line(v_first_name
                             || ' '
                             || v_last_name);
    END;

    dbms_output.put_line(v_first_name
                         || ' '
                         || v_last_name);
END;
/

/* ACCEDER A LOS BLOQUES INTERNOS/EXTERNOS */
<< outer >> 
DECLARE
    v_father_name    VARCHAR2(20) := 'Patrick';
    v_date_of_birth  DATE := '20-Apr-1972';
BEGIN
    DECLARE
        v_child_name     VARCHAR2(20) := 'Mike';
        v_date_of_birth  DATE := '12-Dec-2002';
    BEGIN
        dbms_output.put_line('Father''s Name: ' || v_father_name);
        dbms_output.put_line('Date of Birth: ' || outer.v_date_of_birth);
        dbms_output.put_line('Child''s Name: ' || v_child_name);
        dbms_output.put_line('Date of Birth: ' || v_date_of_birth);
    END;
END;