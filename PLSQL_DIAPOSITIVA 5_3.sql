/* CURSOR FOR LOOPS 
SIMPLIFICAR C�DIGO DE LOS LOOPS
*/
DECLARE
    CURSOR cur_emps IS
    SELECT
        employee_id,
        last_name
    FROM
        employees
    WHERE
        department_id = 50;

BEGIN
    FOR v_emp_record IN cur_emps LOOP
        dbms_output.put_line(v_emp_record.employee_id
                             || ' '
                             || v_emp_record.last_name);
    END LOOP;
END;
/

/* DOS C�DIGOS LOOPS QUE HACEN LO MISMO PERO UNO SIMPLIFICADO */
/* FOOR LOOP */
DECLARE
    CURSOR cur_emps IS
    SELECT
        employee_id,
        last_name
    FROM
        employees
    WHERE
        department_id = 50;

BEGIN
    FOR v_emp_rec IN cur_emps LOOP
        dbms_output.put_line(�);
    END LOOP;
END;
/

/*  */
DECLARE
    CURSOR cur_emps IS
    SELECT
        employee_id,
        last_name
    FROM
        employees
    WHERE
        department_id = 50;

    v_emp_rec cur_emps%rowtype;
BEGIN
    OPEN cur_emps;
    LOOP
        FETCH cur_emps INTO v_emp_rec;
        EXIT WHEN cur_emps%notfound;
        dbms_output.put_line(v_emp_rec.employee_id);
    END LOOP;

    CLOSE cur_emps;
END;
/

/* CURSOR LOOP USANDO SUBQUERIES */
BEGIN
    FOR v_emp_record IN (
        SELECT
            employee_id,
            last_name
        FROM
            employees
        WHERE
            department_id = 50
    ) LOOP
        dbms_output.put_line(v_emp_record.employee_id
                             || ' '
                             || v_emp_record.last_name);
    END LOOP;
END;
/

/* COPIAR TODAS LAS TABLAS DE UN USUARIO A OTRO
USA SUBCONSULTAS
NO EJECUTAR POR AHORA*/
BEGIN 
    FOR c IN (SELECT * FROM USER_TABLES) LOOP
        EXECUTE IMMEDIATE 'CREATE TABLE Luis2.'||c.TABLE_NAME||' AS SELECT * FROM HR.'||c.TABLE_NAME;
    END LOOP;
END;