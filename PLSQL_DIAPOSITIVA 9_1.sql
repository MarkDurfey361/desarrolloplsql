/* FUNCIONES */
CREATE OR REPLACE FUNCTION get_sal (
    p_id IN employees.employee_id%TYPE
) RETURN NUMBER IS
    v_sal employees.salary%TYPE := 0;
BEGIN
    SELECT
        salary
    INTO v_sal
    FROM
        employees
    WHERE
        employee_id = p_id;

    RETURN v_sal;
EXCEPTION
    WHEN no_data_found THEN
        RETURN NULL;
END get_sal;
/

/*ejecutar la funcion*/
DECLARE
    v_sal employees.salary%TYPE;
BEGIN
    --v_sal := get_sal(9997);
                dbms_output.put_line(get_sal(100));
END;
/

/*  */
SELECT
    job_id,
    get_sal(employee_id)
FROM
    employees;
/

/*  */
CREATE OR REPLACE FUNCTION valid_dept (
    p_dept_no departments.department_id%TYPE
) RETURN BOOLEAN IS
    v_valid VARCHAR2(1);
BEGIN
    SELECT
        'x'
    INTO v_valid
    FROM
        departments
    WHERE
        department_id = p_dept_no;

    return(true);
EXCEPTION
    WHEN no_data_found THEN
        return(false);
    WHEN OTHERS THEN
        NULL;
END;

/* */
BEGIN
    IF valid_dept(1000) THEN
        dbms_output.put_line('TRUE');
    ELSE
        dbms_output.put_line('FALSE');
        dbms_output.put_line(user);
    END IF;
END;
/

/**/
SELECT job_id, SYSDATE-(sysdate-5)/*hire_date*/ FROM employees;