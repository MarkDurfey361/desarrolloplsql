/* 
CURSOR EXPL�CITO SOBRE TABLA DEPARTMENTS
SE MUESTRAN ID Y NOMBRE DE LOS DEPARTAMENTOS
*/
DECLARE-- SE DEFINEN LAS VARIABLES
                        CURSOR cur_depts IS -- SE DEFINE UN CURSOR (AQU� EL NOMBRE)
                        SELECT -- C�MO VA A CONSTRUIRSE
                                                department_id,--SE PODR�A USAR A PARTIR DE UNA VISTA
                                                department_name, -- ES UN COLLECTION
                                                location_id
    FROM
        departments;

    v_department_id    departments.department_id%TYPE;--VARIABLE
                        v_department_name  departments.department_name%TYPE;--VARIABLE
                        v_location_id      departments.
    location_id%TYPE;--VARIABLE
BEGIN --AQU� SE EMPIEZA A USAR EL CURSOR
                        OPEN cur_depts; -- SE ABRE O EJECUTA EL CURSOR
                        LOOP-- SE ITERA EN EL CURSOR
                                                FETCH cur_depts INTO-- FETCH RECIBE LOS REGISTROS DEL CURSOR UNO POR UNO
                                                                        v_department_id,-- SE GUARDAN LOS VALORES
                                                                        v_department_name,
            v_location_id;
        EXIT WHEN cur_depts%notfound;-- SALIR DEL LOOP CUANDO YA NO HAYA M�S REGISTROS
                                                dbms_output.put_line(v_department_id
                             || ' '
                             || v_location_id
                             || ' '
                             || v_department_name);-- SE IMPRIMEN LAS VARIABLES
                        END LOOP;-- SE ACABA EL LOOP

                        CLOSE cur_depts;-- SE CIERRA EL FLUJO (UN CURSOR)
END;-- FIN DEL BLOQUE
/

/*NOTA: Siempre que veamos un count, generalmente se acompa�a de un GROUP BY*/
DECLARE
    CURSOR cur_emps IS
    SELECT
        employee_id,
        last_name,
        salary
    FROM
        employees
    WHERE
        department_id = 30;

    v_empno  employees.employee_id%TYPE;
    v_lname  employees.last_name%TYPE;
    v_sal    employees.salary%TYPE;
BEGIN
    OPEN cur_emps;
    LOOP
        FETCH cur_emps INTO
            v_empno,
            v_lname,
            v_sal;
        EXIT WHEN cur_emps%notfound;
        dbms_output.put_line(v_empno
                             || ' '
                             || v_lname);
    END LOOP;

    CLOSE cur_emps;-- SE CIERRA EL FLUJO (UN CURSOR)
END;
/

/*
NOTA: SIEMPRE INDICAR EL CRITERIO DE SALIDA DE UN LOOP
NOTA: UN CURSOR PUEDE SER REABIERTO S�LO SI FUE CERRADO
*/

/*  */
DECLARE
    CURSOR cur_emps IS
    SELECT
        employee_id,
        last_name
    FROM
        employees
    WHERE
        department_id = 10;

    v_empno  employees.employee_id%TYPE;
    v_lname  employees.last_name%TYPE;
BEGIN
    OPEN cur_emps;
    LOOP
        FETCH cur_emps INTO
            v_empno,
            v_lname;
        dbms_output.put_line(v_empno
                             || ' '
                             || v_lname);
        EXIT WHEN cur_emps%notfound;
    END LOOP;

    CLOSE cur_emps;-- SE CIERRA EL FLUJO (UN CURSOR)
END;